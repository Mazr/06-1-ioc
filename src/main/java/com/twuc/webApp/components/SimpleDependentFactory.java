package com.twuc.webApp.components;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration

public class SimpleDependentFactory {
    @Bean
    public SimpleObject create(SimpleDependent dependent)  {
        dependent.setName("O_o");
        return new SimpleObject(dependent);
    }
}
