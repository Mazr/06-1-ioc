package com.twuc.webApp.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MultipleConstructor {
    private Dependency dependency;
    private String s;

    @Autowired
    public MultipleConstructor(Dependency dependency) {
        this.dependency = dependency;
    }

    public MultipleConstructor(String s) {
        this.s = s;
    }

    public Dependency getDependency() {
        return dependency;
    }

    public String getS() {
        return s;
    }
}
