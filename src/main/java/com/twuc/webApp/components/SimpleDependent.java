package com.twuc.webApp.components;


import org.springframework.stereotype.Component;

@Component
public class SimpleDependent {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
