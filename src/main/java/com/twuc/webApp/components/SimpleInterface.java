package com.twuc.webApp.components;

import org.springframework.stereotype.Component;

@Component
public interface SimpleInterface {
    SimpleDependent getSimpleDependent();

}
