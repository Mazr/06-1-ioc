package com.twuc.webApp.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WithAutowiredMethod {
    private Dependency dependency;
    private AnotherDependent anotherDependent;
    private String string;

    public WithAutowiredMethod(Dependency dependency) {
        this.dependency = dependency;
        this.string = "initializa methon be called first";
    }

    public Dependency getDependency() {
        return dependency;
    }

    public AnotherDependent getAnotherDependent() {
        return anotherDependent;
    }

    @Autowired
    void initialize(AnotherDependent another){
        this.anotherDependent = another;
        this.string = "Constuctor be called first";
    }

    public String getString() {
        return string;
    }
}
