package com.twuc.webApp.components;

import org.springframework.stereotype.Component;

@Component
public class WithDependency {
    private Dependency dependence;

    public WithDependency(Dependency dependence) {
        this.dependence = dependence;
    }
}
