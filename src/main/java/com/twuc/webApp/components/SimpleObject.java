package com.twuc.webApp.components;

import org.springframework.stereotype.Component;

public class SimpleObject implements SimpleInterface {
    private SimpleDependent simpleDependent;

    public SimpleObject(SimpleDependent simpleDependent) {
        this.simpleDependent = simpleDependent;
    }

    @Override
    public SimpleDependent getSimpleDependent() {
        return simpleDependent;
    }
}
