package com.twuc.webApp.yourTurn;


import com.twuc.webApp.OutOfScanningScope;
import com.twuc.webApp.components.*;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class IOCTest {
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.components");

    @Test
    void should_create_object_using_() {
        WithoutDependency bean = context.getBean(WithoutDependency.class);
        assertNotNull(bean);
        assertSame(WithoutDependency.class, bean.getClass());
    }

    @Test
    void should_create_object_with_dependency_using_annotated_applicatiuon_context() {
        WithDependency bean = context.getBean(WithDependency.class);
        assertNotNull(bean);
        assertSame(WithDependency.class, bean.getClass());
    }

    @Test
    void should_return_exception_when_create_object_out_of_scope() {
        try {
            OutOfScanningScope bean = context.getBean(OutOfScanningScope.class);

        } catch (Exception e) {
            assertEquals("???", "???");
        }
    }

    @Test
    void should_create_object_with_interface_class() {
        Interface bean = context.getBean(Interface.class);
        assertNotNull(bean);
        assertSame(InterfaceImpl.class, bean.getClass());

    }

    @Test
    void should_create_Object_with_configuration() {
        SimpleInterface bean = context.getBean(SimpleInterface.class);
        assertNotNull(bean);
        assertSame(SimpleObject.class, bean.getClass());
        assertEquals("O_o", bean.getSimpleDependent().getName());
    }

    @Test
    void should_create_object_with_the_first_constructor() {
        MultipleConstructor bean = context.getBean(MultipleConstructor.class);
        assertNotNull(bean);
        assertNotNull(bean.getDependency());
        assertNull(bean.getS());
    }

    @Test
    void should_create_object_list_implement_interface() {
        Map<String, InterfaceWithMultipleImpls> beansOfType = context.getBeansOfType(InterfaceWithMultipleImpls.class);

        assertNotNull(beansOfType);
        assertEquals(ImplementationA.class,beansOfType.get("implementationA").getClass());
        assertEquals(ImplementationB.class,beansOfType.get("implementationB").getClass());
        assertEquals(ImplementationC.class,beansOfType.get("implementationC").getClass());

    }

    @Test
    void should_create_object_and_call_methon() {
        WithAutowiredMethod bean = context.getBean(WithAutowiredMethod.class);
        assertNotNull(bean);
        assertEquals("Constuctor be called first", bean.getString());
        assertNotNull(bean.getAnotherDependent());
        assertNotNull(bean.getDependency());


    }
}
